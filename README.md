# Guidelines

CSCM603228 - IT Project @ Faculty of Computer Science Universitas
Indonesia, Odd Semester 2017/2018

* * *

This is a repository for storing documents related to activities
in IT Project course.

## Table of Contents

- [Definition of Done](definition-of-done.md)
- [Backlog Chores for Sprint 1](backlog-chores.md)
- [Scrum Guide](scrum.md)
- [Git Guide](#)
- [Programming Guide](#)
- [Deployment Guide](#)

## Credits

All documents are translated from Bahasa Indonesia version written
by PPL Even Semester 2016/2017 teaching team.
