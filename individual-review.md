# Individual Review Guidelines

CSCM603228 - IT Project @ Faculty of Computer Science Universitas
Indonesia, Odd Semester 2017/2018

* * *

This document describes rundown of individual review session that
will be conducted fortnightly in this course. Each group has been
scheduled to attend review session with teaching team (lecturer
and/or teaching assistant) where each member in the group have to
explain their learning achievement(s) pertaining to **four sets of
competences** as described in separate individual learning
evaluation sheet.

## Rundown

There are **2** individual review sessions in each sprint that
shall be conducted during class sessions. Refer to the course
profile for the exact date.

The schedule for each group is as follows:

- KI1 (Rokujin) : **Tuesday, 16:00 - 17:00**
- KI2 (DreaM) : **Tuesday, 17:00 - 18:00**
- KI3 (LulusCepatClub) : **Thursday, 16:00 - 17:00**

During review sessions, each student has **at most 10 minutes**
to describe and elaborate their learning achievement(s). Student
is recommended to bring displayable proofs (e.g. code example,
blog post) to speed up elaboration process.

The protocol between a student and teaching team during review
session is as follows:

1. Student mentions number of competences that will be evaluated
2. For each competence:
    1. Teaching team asks for proof of achievement and desired
    grade for that particular competence
    2. Student provides the proof and elaborate their achievement
    3. Teaching team evaluates and gives the grade related to
    the competence
3. If time permitting, teaching team can ask student to elaborate
competences that have not been described

## Behavioural Attributes Evaluation

There is a set of competences related to behavioural attributes
(i.e. behaviour, attitude, soft skills) that will be evaluated
during individual review session. To help students prepare for
evaluation of said competences, there are six questions that
need to be answered by students during review session:

- [Initiative] Has there been any initiative done by team?
- [Commitment] What are the commitment and work hours 
- [Work Ethics] How is the work ethics in the team?
- [Culture Sensitivity] How are the team dynamics and team work in
the team?
- [Knowledge Sharing] What has been shared with team members?
- [Communication] Explain answers to above questions in clear and
succinct way
