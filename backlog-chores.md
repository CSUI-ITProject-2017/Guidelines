# Backlog Chores for Sprint 1

CSCM603228 - IT Project @ Faculty of Computer Science Universitas
Indonesia, Odd Semester 2017/2018

* * *

- Prepare environment (including DBMS - database if required) for
staging environment on Heroku
- Prepare local development environment (including DBMS - database
if required) on each team member PC/laptop
- Prepare Git Flow-related (including user stories) branches for
Sprint 1 on GitLab
- Prepare build tool, including the script and required
dependencies
- Prepare test suite and test runner that recognisable by GitLab
- Prepare code coverage tool and integrate it to GitLab CI/CD
pipeline view
- Prepare GitLab CI Runner that execute build and test scripts
- Prepare provisioning and deployment scripts to deploy and run
the application on staging environment
- Prepare initial data and ask for sample data if available
- Prepare scripts to migrate data
