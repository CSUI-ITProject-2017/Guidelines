# Definition of Done

CSCM603228 - IT Project @ Faculty of Computer Science Universitas
Indonesia, Odd Semester 2017/2018

* * *

The following are the conditions that must be met in order to
consider a user story has been completed:

- [ ] Passed all unit tests (i.e. *green* pipeline status on
GitLab)
- [ ] Reached minimum code coverage: **80%**
- [ ] Provided sample data for demo, testing, and evaluation on
staging environment
- [ ] Separated database (including the migration and deployment
scripts) for testing/staging and production environment
- [ ] Deployed to staging environment
- [ ] Implemented exception/error handling and input validation
- [ ] Each commit related to a user story in `develop` branch has
been reviewed at least by 2 (two) team members
- [ ] All issues on GitLab that are related to user story have
been closed
