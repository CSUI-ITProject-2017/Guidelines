# Scrum Guidelines

CSCM603228 - IT Project @ Faculty of Computer Science Universitas
Indonesia, Odd Semester 2017/2018

* * *

This document serves as guidelines on implementing Scrum methodology in IT
Project course. It was written based on inputs from industry and existing
written literatures on Scrum. In addition, the guidelines were designed for
ease of use throughout the course and adjusted to accomodate course schedule
and needs.

All participants are still required to read and understand the main references
used in this document. Understanding the main references will provide great
benefits in Software Engineering career in the future.

The main references are as follows:

1. [The Scrum Guide - The Definitive Guide to Scrum - The Rules of the Game](http://www.scrumguides.org)
2. [Scrum Body of Knowledge (SBOK) Guide, 2013 Edition](http://www.scrumstudy.com/SBOKGuide)

## Methodology

The Scrum roles assignment in this course is as follows:

- Product Owner: topic owner (i.e. client) or lecturer
- Scrum Master: teaching assistant
- Scrum Team: all members in a group

There will be **3** sprints where each sprint spans for **4** weeks.

## Checklist

1. Initiation Phase (Sprint 0)
    1. Create **Project Vision** and submit it via Scele (week 1 & 2)
    2. Create **Epic(s)** during ideation activities (week 1 & 2)
    3. Create **Product Backlog** (user stories) (week 3)
        - Elaboration of epics
        - User story format: `As a _SUBJECT_, I want to be able to _WHAT_, so I can _WHY_`
        - Remember INVEST (Independent, Negotiable, Valuable, Estimable, Small,
        Testable) criteria when writing user stories
        - Define quantifiable **Acceptance Criteria** related to
        non-functional requirements on each user story
        - Define minimum **Definition of Done** that applicable to all user stories
    4. Release Product Backlog on project management tool, e.g. issue tracker on
    a GitLab instance
2. Setup Project Management Tool
    1. Use [GitLab](https://www.gitlab.com) as project management tool
        - Project repository must be set to Public unless the client/product
        owner requested it to be Private
    2. Create at least **3** milestones where each milestone represents a
    sprint
        - Refer to class schedule to set the start date & end date of each
        sprint
    3. Create at least **4** labels in the issue tracker
        - Bug --> Errors/faults found in the current product increment
        - Chore --> Tasks that are not directly related to user stories but
        necessary in order to support the whole development process
        - Story --> Self-explanatory
        - Task --> Tasks related to one or more user stories
    4. Transfer items in Product Backlog into issue tracker as set of
    **issues**
        - Assign correct label to each issue, e.g. a user story must be
        labelled as Story
    5. Do not forget to include minimal chore tasks related to setting up
    development environment and CI/CD pipeline as defined in
    [Backlog Chores for Sprint 1 document](backlog-chores.md).
3. Sprint Planning
    1. Estimate the weights for all user stories using Fibonacci scale
    (0, 1, 2, 3, 5, 8, 13, 21)
    2. Decompose (or breakdown) user stories into smaller tasks
        - A task should be doable in a single workday (8 hours)
        - Put tasks into issue tracker
            - Use correct label, i.e. `Task`
            - Associate the tasks with its respective user story by
            mentioning the ID number of its user story in the issue
            description and/or Related Issues section
    3. Define the Sprint Goal
        - Product Owner is responsible on defining a Sprint Goal
    4. Prepare Sprint Backlog, i.e. stories and tasks that will be done
    in current sprint
        - The team shall choose one or more user stories from Product
        Backlog that aligned with current Sprint Goal and have been
        prioritised by Product Owner
        - Assign the correct (sprint) milestone to each user stories and
        associated tasks on GitLab
            - Use bulk edit feature on GitLab to make assignment process
            easier
        - Each user story and associated tasks must have a PIC
        (person-in-charge) that is represented as the assignee of the
        particular user story on GitLab Issue Tracker
4. Development Phase
    1. All forms of work (implementation, code) pertaining to a user story
    must be stored into Git repository on GitLab
    2. Determine deliverables for each user story that being worked on
    in current sprint
    3. Conduct Daily Standup Meeting (DSM) at least **2** times a week
    4. Update and maintain the backlog if needed as long it is still
    align with current Sprint Goal
5. Sprint Review & Retrospective
    1. Make sure that **a working software product** is available and
    can be demonstrated during Sprint Review
    2. Demonstration and meeting during Sprint Review shall be conducted
    in front of lecturer, topic owner/client, and teaching assistant
    3. Product Owner shall decide the approval and acceptance of current
    product increment
    4. Sprint Retrospective shall be conducted immediately after Sprint Review
        - Discuss lessons learned by doing silent writing, post ideas, group
        ideas, prioritise, and create a plan
        - There will be a peer review as well during retrospective session
    5. Evaluate Burndown Chart to help planning workload for next Sprint
